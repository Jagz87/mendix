/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = true;
var seriesFilter = "";
var filtersOnlySampleSeries = false;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 99.6237543217409, "KoPercent": 0.3762456782591011};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.7259940697939633, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.9981481481481481, 500, 1500, "65 /xas/"], "isController": false}, {"data": [0.4880952380952381, 500, 1500, "03_create_Expense"], "isController": true}, {"data": [0.9990079365079365, 500, 1500, "134 /xas/"], "isController": false}, {"data": [0.0, 500, 1500, "04_submit_Expense"], "isController": true}, {"data": [0.9990253411306043, 500, 1500, "121 /xas/"], "isController": false}, {"data": [0.0018315018315018315, 500, 1500, "02_login_Manager"], "isController": true}, {"data": [0.49423076923076925, 500, 1500, "05_reimburse_Expense"], "isController": true}, {"data": [1.0, 500, 1500, "63 /xas/"], "isController": false}, {"data": [1.0, 500, 1500, "73 /xas/"], "isController": false}, {"data": [0.9847328244274809, 500, 1500, "04_approve_Expense"], "isController": true}, {"data": [0.0, 500, 1500, "02_login_Employee"], "isController": true}, {"data": [0.9990196078431373, 500, 1500, "123 /xas/"], "isController": false}, {"data": [0.5629932356257046, 500, 1500, "bzm - Parallel Controller"], "isController": false}, {"data": [0.9230769230769231, 500, 1500, "01_launch_Manager"], "isController": true}, {"data": [0.9612403100775194, 500, 1500, "117 /login.html"], "isController": false}, {"data": [0.9554263565891473, 500, 1500, "01_launch_Employee"], "isController": true}, {"data": [0.0, 500, 1500, "03_retrieve_Expense"], "isController": true}, {"data": [0.9230769230769231, 500, 1500, "49 /login.html"], "isController": false}, {"data": [0.9823874755381604, 500, 1500, "122 /index.html"], "isController": false}, {"data": [0.9981617647058824, 500, 1500, "56 /xas/"], "isController": false}, {"data": [0.9980916030534351, 500, 1500, "72 /xas/"], "isController": false}, {"data": [1.0, 500, 1500, "135 /xas/"], "isController": false}, {"data": [1.0, 500, 1500, "66 /pages/en_US/Expenses/Desktop_ManagerDashboard.page.xml"], "isController": false}, {"data": [0.9981684981684982, 500, 1500, "54 /xas/"], "isController": false}, {"data": [0.9725609756097561, 500, 1500, "/xas/"], "isController": false}, {"data": [0.9981684981684982, 500, 1500, "55 /index.html"], "isController": false}, {"data": [0.9962962962962963, 500, 1500, "64 /xas/"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 9834, 37, 0.3762456782591011, 644.942241203987, 22, 95338, 1269.0, 2370.0, 2443.6499999999996, 2.486039726581134, 57.85950834434279, 6.426244482155953], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Throughput", "Received", "Sent"], "items": [{"data": ["65 /xas/", 270, 0, 0.0, 121.92222222222232, 26, 546, 149.9, 190.0, 450.18000000000086, 0.06849556592685536, 0.03137150431610856, 0.06448215386082869], "isController": false}, {"data": ["03_create_Expense", 504, 0, 0.0, 1323.64880952381, 626, 55518, 849.0, 965.25, 26494.8, 0.12766209666756334, 0.3680476558300797, 0.4423811635906181], "isController": true}, {"data": ["134 /xas/", 504, 0, 0.0, 122.31349206349203, 69, 524, 159.0, 197.5, 304.1499999999996, 0.1276993592228582, 0.15815642443061034, 0.11248517775294735], "isController": false}, {"data": ["04_submit_Expense", 492, 0, 0.0, 2312.469512195125, 1687, 52146, 2251.4, 2408.749999999999, 2923.6099999999733, 0.12514976000244193, 0.5307552315842892, 3.3903376221609194], "isController": true}, {"data": ["121 /xas/", 513, 0, 0.0, 129.6549707602339, 37, 530, 160.0, 191.5999999999999, 410.9600000000012, 0.1302207609969275, 0.08264354974750372, 0.0590727174432867], "isController": false}, {"data": ["02_login_Manager", 273, 0, 0.0, 2248.135531135531, 1472, 74935, 1918.9999999999998, 2084.0, 25643.81999999976, 0.06978604314924729, 0.6151402849647888, 0.3391437636744218], "isController": true}, {"data": ["05_reimburse_Expense", 260, 0, 0.0, 1341.6, 976, 37867, 1098.8, 1213.95, 20440.679999999746, 0.06637038247209254, 0.2540728739620055, 0.23334035593670308], "isController": true}, {"data": ["63 /xas/", 270, 0, 0.0, 123.58888888888885, 67, 417, 166.0, 204.34999999999997, 321.54000000000053, 0.06854352374131877, 0.049399035928991954, 0.05361656495780892], "isController": false}, {"data": ["73 /xas/", 262, 0, 0.0, 125.39312977099243, 37, 488, 156.40000000000003, 180.39999999999998, 287.7900000000003, 0.06667845840422161, 0.05950256112404102, 0.05964596474440136], "isController": false}, {"data": ["04_approve_Expense", 262, 0, 0.0, 378.31297709923666, 130, 17650, 310.0, 358.0, 5398.060000000055, 0.0666433160696621, 0.14122407444643442, 0.11799251175224355], "isController": true}, {"data": ["02_login_Employee", 513, 0, 0.0, 4265.153996101363, 2640, 95338, 3644.8, 3835.2, 47763.000000000124, 0.12988344669651708, 51.58772643362335, 1.2169575949851823], "isController": true}, {"data": ["123 /xas/", 510, 0, 0.0, 121.87254901960787, 31, 515, 150.90000000000003, 174.45, 354.6999999999996, 0.12938783318001298, 0.5030360811450721, 0.08911269883231286], "isController": false}, {"data": ["bzm - Parallel Controller", 3548, 0, 0.0, 1096.9695603156738, 476, 19319, 2373.1, 2389.0, 2491.0, 0.8972685657839733, 1.7086793090418775, 2.0285613100380275], "isController": false}, {"data": ["01_launch_Manager", 273, 21, 7.6923076923076925, 122.99267399267406, 62, 485, 162.0, 186.90000000000003, 421.45999999999935, 0.0704598862576122, 0.07143379093545177, 0.02518390465848248], "isController": true}, {"data": ["117 /login.html", 516, 16, 3.10077519379845, 258.9573643410853, 62, 1217, 320.3, 403.3499999999998, 624.0199999999985, 0.13106179868602927, 3.0582671352983204, 0.23770726528127234], "isController": false}, {"data": ["01_launch_Employee", 516, 16, 3.10077519379845, 302.6899224806202, 62, 9254, 322.3, 424.15, 951.3099999999974, 0.13124020467367734, 3.0624301574316544, 0.23803084087582996], "isController": true}, {"data": ["03_retrieve_Expense", 270, 0, 0.0, 3652.8666666666677, 2073, 76101, 2885.9, 3006.95, 49456.74000000041, 0.06842756992410623, 0.5727480165983743, 0.3945930466691233], "isController": true}, {"data": ["49 /login.html", 273, 21, 7.6923076923076925, 122.99267399267406, 62, 485, 162.0, 186.90000000000003, 421.45999999999935, 0.0701789563386636, 0.07114897797074848, 0.02508349416010828], "isController": false}, {"data": ["122 /index.html", 511, 0, 0.0, 332.657534246575, 61, 1176, 416.8, 481.79999999999984, 963.4399999999998, 0.12985583968826467, 50.64555409205674, 0.4201116741162243], "isController": false}, {"data": ["56 /xas/", 272, 0, 0.0, 127.55882352941181, 33, 520, 151.40000000000003, 195.39999999999986, 475.9399999999996, 0.06920085982068327, 0.27402599352526735, 0.04737881708253754], "isController": false}, {"data": ["72 /xas/", 262, 0, 0.0, 137.70610687022904, 82, 790, 173.70000000000002, 194.85, 300.4200000000003, 0.06673350205192784, 0.08186350685763147, 0.058456983731034445], "isController": false}, {"data": ["135 /xas/", 504, 0, 0.0, 114.95833333333336, 24, 417, 143.0, 166.75, 355.0999999999998, 0.12769036505256587, 0.07668903760481251, 0.11235255753160338], "isController": false}, {"data": ["66 /pages/en_US/Expenses/Desktop_ManagerDashboard.page.xml", 267, 0, 0.0, 114.97378277153567, 24, 357, 150.40000000000003, 202.0, 343.28, 0.06773187512779001, 0.29646791311814213, 0.04444904305261219], "isController": false}, {"data": ["54 /xas/", 273, 0, 0.0, 128.28937728937726, 41, 532, 164.2, 185.3, 370.2999999999986, 0.06984607000725325, 0.04432813893163656, 0.03172770322519985], "isController": false}, {"data": ["/xas/", 492, 0, 0.0, 320.80284552845563, 154, 838, 426.7, 509.44999999999976, 805.28, 0.1253581433745546, 0.11982659785451576, 2.8583049093191804], "isController": false}, {"data": ["55 /index.html", 273, 0, 0.0, 112.93406593406596, 24, 531, 152.2, 178.3, 364.31999999999925, 0.06958810976262553, 0.0756937972778559, 0.035876141624166094], "isController": false}, {"data": ["64 /xas/", 270, 0, 0.0, 117.43333333333338, 22, 543, 149.0, 180.79999999999995, 479.10000000000184, 0.06851572854677379, 0.0415510424097134, 0.05673958770279704], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["Response was null", 37, 100.0, 0.3762456782591011], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 9834, 37, "Response was null", 37, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["117 /login.html", 516, 16, "Response was null", 16, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["49 /login.html", 273, 21, "Response was null", 21, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
