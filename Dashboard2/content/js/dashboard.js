/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = true;
var seriesFilter = "";
var filtersOnlySampleSeries = false;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 99.84748347737671, "KoPercent": 0.1525165226232842};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.7237002128306476, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "65 /xas/"], "isController": false}, {"data": [0.4912109375, 500, 1500, "03_create_Expense"], "isController": true}, {"data": [1.0, 500, 1500, "134 /xas/"], "isController": false}, {"data": [0.0, 500, 1500, "04_submit_Expense"], "isController": true}, {"data": [1.0, 500, 1500, "121 /xas/"], "isController": false}, {"data": [0.0018726591760299626, 500, 1500, "02_login_Manager"], "isController": true}, {"data": [0.49606299212598426, 500, 1500, "05_reimburse_Expense"], "isController": true}, {"data": [0.9962264150943396, 500, 1500, "63 /xas/"], "isController": false}, {"data": [1.0, 500, 1500, "73 /xas/"], "isController": false}, {"data": [0.9844357976653697, 500, 1500, "04_approve_Expense"], "isController": true}, {"data": [0.0, 500, 1500, "02_login_Employee"], "isController": true}, {"data": [0.998062015503876, 500, 1500, "123 /xas/"], "isController": false}, {"data": [0.5713082537900056, 500, 1500, "bzm - Parallel Controller"], "isController": false}, {"data": [0.9592592592592593, 500, 1500, "01_launch_Manager"], "isController": true}, {"data": [0.9769230769230769, 500, 1500, "117 /login.html"], "isController": false}, {"data": [0.975, 500, 1500, "01_launch_Employee"], "isController": true}, {"data": [0.0, 500, 1500, "03_retrieve_Expense"], "isController": true}, {"data": [0.9703703703703703, 500, 1500, "49 /login.html"], "isController": false}, {"data": [0.7978723404255319, 500, 1500, "122 /index.html"], "isController": false}, {"data": [1.0, 500, 1500, "56 /xas/"], "isController": false}, {"data": [0.9980544747081712, 500, 1500, "72 /xas/"], "isController": false}, {"data": [1.0, 500, 1500, "135 /xas/"], "isController": false}, {"data": [1.0, 500, 1500, "66 /pages/en_US/Expenses/Desktop_ManagerDashboard.page.xml"], "isController": false}, {"data": [1.0, 500, 1500, "54 /xas/"], "isController": false}, {"data": [0.974155069582505, 500, 1500, "/xas/"], "isController": false}, {"data": [1.0, 500, 1500, "55 /index.html"], "isController": false}, {"data": [1.0, 500, 1500, "64 /xas/"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 9835, 15, 0.1525165226232842, 623.7323843416365, 22, 95328, 1263.0, 2369.0, 2419.6399999999994, 2.48679671505153, 59.25427228941686, 6.422081698383544], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Throughput", "Received", "Sent"], "items": [{"data": ["65 /xas/", 262, 0, 0.0, 112.54580152671754, 25, 379, 135.10000000000005, 158.09999999999997, 258.43000000000063, 0.06651235124208008, 0.030463176496616756, 0.06261514316148944], "isController": false}, {"data": ["03_create_Expense", 512, 0, 0.0, 1165.236328125, 617, 55502, 797.4, 931.1999999999996, 16804.630000000012, 0.12967520918991998, 0.3738476168444551, 0.44939783103649494], "isController": true}, {"data": ["134 /xas/", 512, 0, 0.0, 119.72070312500004, 68, 444, 142.7, 169.44999999999976, 299.35, 0.12969225699229978, 0.16062715325242596, 0.11424064043657657], "isController": false}, {"data": ["04_submit_Expense", 503, 0, 0.0, 2466.268389662027, 1690, 53606, 2196.4, 2362.0, 14471.839999999956, 0.1273630853163023, 0.5380484042709426, 3.3426314049800485], "isController": true}, {"data": ["121 /xas/", 519, 0, 0.0, 123.13102119460511, 37, 363, 150.0, 178.0, 284.7999999999988, 0.13185238629954626, 0.08367898802658388, 0.059482382129936846], "isController": false}, {"data": ["02_login_Manager", 267, 0, 0.0, 2521.314606741573, 1461, 79599, 1853.4, 1979.6, 48828.23999999996, 0.06775346265870678, 0.6002996738666775, 0.3285656849678412], "isController": true}, {"data": ["05_reimburse_Expense", 254, 0, 0.0, 1119.1299212598428, 977, 19141, 1070.5, 1143.0, 3466.2999999999474, 0.06461568675536145, 0.2472419027036577, 0.22671585890910403], "isController": true}, {"data": ["63 /xas/", 265, 0, 0.0, 122.15849056603777, 70, 539, 146.0, 175.19999999999993, 469.3199999999988, 0.0673019462453006, 0.04852406831884056, 0.05264537006102128], "isController": false}, {"data": ["73 /xas/", 255, 0, 0.0, 124.80392156862736, 34, 401, 151.0, 180.7999999999999, 343.55999999999983, 0.06496893083815271, 0.0581398781240183, 0.058116738913816295], "isController": false}, {"data": ["04_approve_Expense", 257, 0, 0.0, 343.22568093385206, 167, 19990, 308.20000000000005, 368.4999999999999, 1116.9799999999898, 0.06545292661173376, 0.13841744007873708, 0.11542927831972405], "isController": true}, {"data": ["02_login_Employee", 519, 0, 0.0, 4102.161849710981, 3078, 95328, 3751.0, 3871.0, 25784.999999999516, 0.1312851832908111, 53.083558025221045, 1.2378297872263058], "isController": true}, {"data": ["123 /xas/", 516, 0, 0.0, 123.47093023255815, 33, 1142, 147.3, 166.14999999999998, 332.8999999999995, 0.13075807500800132, 0.5083497824911644, 0.08996546757795247], "isController": false}, {"data": ["bzm - Parallel Controller", 3562, 0, 0.0, 1087.8941605839448, 475, 14092, 2372.0, 2383.85, 2457.74, 0.9009906850412781, 1.7113249823539127, 2.0357254299814893], "isController": false}, {"data": ["01_launch_Manager", 270, 8, 2.962962962962963, 170.59259259259258, 97, 5439, 140.8, 174.39999999999986, 4342.04000000003, 0.0689740090606302, 0.07269563184407378, 0.024031634450269485], "isController": true}, {"data": ["117 /login.html", 520, 7, 1.3461538461538463, 254.6942307692308, 66, 917, 303.0, 336.84999999999997, 811.5499999999984, 0.13246534566449839, 3.1464134231497205, 0.2433102910766758], "isController": false}, {"data": ["01_launch_Employee", 520, 7, 1.3461538461538463, 267.30000000000007, 66, 6794, 303.0, 337.95, 832.8499999999995, 0.13287532231848018, 3.156151487401886, 0.24406332983194082], "isController": true}, {"data": ["03_retrieve_Expense", 263, 0, 0.0, 3226.098859315592, 2440, 58127, 2797.6, 2907.6, 27964.640000000203, 0.06669026606119681, 0.5580577535326823, 0.38423371743207485], "isController": true}, {"data": ["49 /login.html", 270, 8, 2.962962962962963, 117.36296296296298, 97, 426, 137.0, 158.89999999999998, 332.63000000000096, 0.06897399144058312, 0.07269561327330383, 0.024031628311166713], "isController": false}, {"data": ["122 /index.html", 517, 0, 0.0, 451.8607350096714, 103, 985, 568.2, 605.5999999999998, 803.2800000000002, 0.1311192690798192, 52.07609562933128, 0.4305320597867093], "isController": false}, {"data": ["56 /xas/", 266, 0, 0.0, 120.82706766917295, 32, 357, 148.0, 176.0, 308.90999999999957, 0.06767244197533313, 0.26796631384774056, 0.0462077880422795], "isController": false}, {"data": ["72 /xas/", 257, 0, 0.0, 134.1634241245136, 86, 531, 159.0, 179.39999999999998, 260.1199999999989, 0.06547050108115489, 0.08032182230197848, 0.0573506244822226], "isController": false}, {"data": ["135 /xas/", 510, 0, 0.0, 111.16666666666653, 23, 400, 132.0, 158.0, 349.9399999999994, 0.12918944263874754, 0.07758936252229469, 0.11367157013428864], "isController": false}, {"data": ["66 /pages/en_US/Expenses/Desktop_ManagerDashboard.page.xml", 260, 0, 0.0, 109.28461538461539, 22, 280, 133.9, 154.0, 203.94999999999993, 0.06602156823062451, 0.28898025275849537, 0.04332665415134734], "isController": false}, {"data": ["54 /xas/", 267, 0, 0.0, 123.62546816479403, 42, 423, 149.60000000000005, 169.0, 231.95999999999964, 0.06834239488645308, 0.043372404268814446, 0.030590200730828486], "isController": false}, {"data": ["/xas/", 503, 0, 0.0, 318.52882703777357, 167, 1368, 401.6, 512.1999999999994, 911.3999999999967, 0.1274052489949423, 0.12183013152185695, 2.800288328384243], "isController": false}, {"data": ["55 /index.html", 267, 0, 0.0, 110.70037453183521, 26, 419, 143.0, 174.19999999999996, 332.8399999999999, 0.06807591458843903, 0.07701789990201657, 0.03481555371151416], "isController": false}, {"data": ["64 /xas/", 263, 0, 0.0, 110.06083650190112, 26, 357, 139.2, 183.79999999999998, 327.6800000000002, 0.06676328303748061, 0.04048828004519087, 0.05528834376541362], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["Response was null", 15, 100.0, 0.1525165226232842], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 9835, 15, "Response was null", 15, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["117 /login.html", 520, 7, "Response was null", 7, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["49 /login.html", 270, 8, "Response was null", 8, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
